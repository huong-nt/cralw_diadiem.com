# encoding: UTF-8
require_relative "../initialize.rb"
require_relative ROOT_DIRECTORY+"config/DBConfig.rb"
class CrawlLozi
  def initialize(_citynames)
    @city = _citynames
    # @baseLink = _link
    @con = Connector.makeConnect(DBConfig::HOSTNAME, DBConfig::DB_USER, DBConfig::DB_PASS, DBConfig::DB_NAME)
    @source = "lozi"
    @linkTable = "#{@city}_#{@source}_link"
    @detailTable = "#{@city}_#{@source}_detail"
  end

  def createTable
    hash = {
      "link" => "varchar(500)",
      "state" => "varchar(10)",
    }

    Crawl.createTableByHash(@con, @linkTable, hash)

    hash = {
      "name"           => "varchar(500)",
      "address"        => "varchar(500)", 
      "phone"          => "varchar(200)",
      "area"           => "varchar(100)",
      "near"           => "varchar(100)",
      "derection"      => "text",
      "pricerange"     => "varchar(100)",
      "timeopen"       => "varchar(100)",
      "convenient"     => "text",
      "type"           => "varchar(100)",
      "appropriate"    => "varchar(500)",
      "highlight_dish" => "varchar(200)",
      "description"    => "text",
      "style"          => "varchar(200)",
      "capacity"       => "varchar(100)",
      "longtitude"     => "varchar(50)",
      "latitude"       => "varchar(50)",
      "images"         => "text",
      "link"           => "varchar(300)"
    }
    Crawl.createTableByHash(@con, @detailTable, hash)
  end

  def getLink
    # Empty table link
    @con.query ("truncate table `#{@linkTable}`") 

    wait                                      = Selenium::WebDriver::Wait.new(:timeout => 15)  # seconds
    profile                                   = Selenium::WebDriver::Firefox::Profile.from_name "default"
    profile['permissions.default.stylesheet'] = 2
    driver                                    = Selenium::WebDriver.for(:firefox, :profile => profile) #
    # Get first link from database
    res_arg = @con.query ("select * from `#{@source}_arguments` where `province` = '#{@city}' ")
    i = 0 
    count_page = 1
    count = 12;
    res_arg.each_hash do |arg_row|
      initialink = arg_row['argument']
      initialink = "http://lozi.vn/tim-kiem?q=Qu%E1%BA%A3ng+Ninh&price%5B0%5D%5Bmin%5D=0&price%5B0%5D%5Bmax%5D=1000000&tab=eatery&tags%5B%5D=sang-trong&min=0&max=Infinity"
      
      driver.navigate.to initialink
      sleep(1)
          while true
            begin
              element = driver.find_element(:xpath, "//div[@class='view-more']")
              element.click()
              puts count_page = count_page + 1
              sleep(5)
              rescue Exception => e
                puts e.message
                break
            end
          end
          string = driver.page_source
           wait.until{
            begin
              doc = Nokogiri::HTML(string)  
              
              doc.css("a[class='primary-info']").map { |item|  
                hash = Hash.new
                puts hash['link'] = "http://www.lozi.vn" + item.attr("href")
                puts  i = i+1
                Crawl.genDataInsert(@con, hash, @linkTable)
              }
              break
            rescue Exception => e
              puts e.message
              puts e.backtrace.inspect
            end
           }
           puts count_page
           puts "------------------------"
      
    end
    puts "Finish get link!"
    driver.quit

  end

  def getDetail
    # Empty table detail
    @con.query ("truncate table `#{@detailTable}`")

      #------ Test key:
      # comment toan bo code phia duoi cua function nay
      # uncomment 2 dong code duoi day
      link = "http://lozi.vn/nha-hang/ha-noi-ha-noi-43-trang-tien-quan-hoan-kiem-ha-noi-127"
      #link = "http://lozi.vn/nha-hang/churro-vietnam-29-nha-chung"
      getDetailOfALink(link)
      #----------------------------------------------------------------

    # res = @con.query("select * from `#{@linkTable}` where id >= 0")
    # res.each_hash do |row|
    #   id = row['id']
    #   link = row['link']
    #   puts link
    #   if link == nil
    #     break
    #   end

    #   getDetailOfALink(link)
    # @con.query("update `#{@linkTable}` set state = '1' where id = '#{id}' ")
    # end
  end

  def getDetailOfALink(link)
    link_ori = link
    doc = Crawl.genNokogiri(link)
    hash = Hash.new
    name = getName(doc)
    if(name != nil)
      hash['name'] = name
      hash['address'] = getAddress(doc)
      hash['phone'] = getPhone(doc)
      hash['area'] = getArea(doc)
      hash['near'] = getNear(doc)
      hash['derection'] = getDerection(doc)
      hash['pricerange']= getPricerange(doc)
      hash['timeopen']= getTimeopen(doc)
      hash['convenient'] = getConvenient(doc)
      hash['type'] = getType(doc)
      hash['appropriate'] = getAppropriate(doc)
      hash['highlight_dish'] = getHighlightDisk(doc)
      hash['description'] = getDescription(doc)
      hash['style'] = getStyle(doc)
      hash['capacity'] = getCapacity(doc)
      puts hash['images'] = getImages(doc)
      hash['latitude'] = getLatitude(doc)
      hash['longtitude'] = getLongtitude(doc)
      hash['link'] = link
      Crawl.genDataInsert(@con, hash, @detailTable)
    end
  end

  def getName(doc)
    begin
      name =  doc.css("meta[property='og:title']").attr("content")
      return name
    rescue Exception => e
      puts e
      return nil
    end
  end

  def getAddress(doc)
    address =  doc.css("div[class='info address']").inner_text
    return MyString.stripString(address)
  end

  def getPhone(doc)
    begin
    phone = doc.css("div[class='info phone-number']").inner_text
    rescue Exception => e
      return nil
    end
    puts e
    return MyString.stripString(phone)
  end
  
  def getArea(doc)
    area = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Khu vực")
            area = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return area
    rescue Exception => e
      puts e
      return nil
    end 
  end
  
  def getNear(doc)
    near = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Ở gần")
            near = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return near
    rescue Exception => e
      return nil
    end
  end

  def getDerection(doc)
    str = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Chỉ đường")
            str = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return str
    rescue Exception => e
      return nil
    end 
  end  

  def getPricerange(doc)
    string = ""
    begin
      string = doc.css("dl[class='details']").inner_text
      string = string.split("Khoảng giá")[1]
      range1 = string.split("~")[0]
      range2 = string.split("~")[1]
      range2 = range2.split("đ")[0]
      price = range1 + "~" + range2 + "đ"
      return price
    rescue Exception => e
      return nil
    end
  end

  def getTimeopen(doc)
    str = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Thời gian phục vụ")
            str = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return str
    rescue Exception => e
      return nil
    end 
  end

  def getConvenient(doc)
    str = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Tiện nghi")
            str = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return str
    rescue Exception => e
      return nil
    end 
  end
  def getType(doc)
    str = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Thể loại nhà hàng")
            str = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return str
    rescue Exception => e
      return nil
    end 
  end

  def getAppropriate(doc)
    str = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Thích hợp")
            str = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return str
    rescue Exception => e
      return nil
    end 
  end
  def getHighlightDisk(doc)
    str = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Món ăn nổi bật")
            str = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return str
    rescue Exception => e
      return nil
    end 
  end

  def getDescription(doc)
    str = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Nổi trội")
            str = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return str
    rescue Exception => e
      return nil
    end 
  end
  def getStyle(doc)
    str = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Phong cách ẩm thực")
            str = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return str
    rescue Exception => e
      return nil
    end 
  end
  
  def getCapacity(doc)
    str = ""
    i = 0
    begin
      doc.css("dl[class='details']").css("dt").map { |item|  
          if(item.inner_text == "Sức chứa")
            str = doc.css("dl[class='details']").css("dd")[i].inner_text
          end
          i = i + 1
      }
      return str
    rescue Exception => e
      return nil
    end 
  end

  def getLatitude(doc)
    lat = doc.css("meta[property='place:location:latitude']").attr("content")
    return lat  
  end
  def getLongtitude(doc)
    lng = doc.css("meta[property='place:location:longitude']").attr("content")
    return lng 
  end
  
  def getImages(doc)
    image = ""
    begin
      i = 0
    doc.css("ul[class='photos']").css("li").map{|item|
      str = item.css("img").attr("style")
      str = str.to_s
      begin
        str = str.split("('")[1]
        str = str.split("-")[0]
      rescue Exception => e
        return image
      end
      str = str + "-o-720.jpg"
      image = image + " | " + str
      i = i + 1
      if(i == 3)
        return image
      end
    }
    return image
    rescue Exception => e
      return nil
    end
  end
end  