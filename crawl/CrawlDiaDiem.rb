# encoding: UTF-8
require_relative "../initialize.rb"
require_relative ROOT_DIRECTORY+"config/DBConfig.rb"
class CrawlDiaDiem
  def initialize(_citynames)
    @city = _citynames
    # @baseLink = _link
    @con = Connector.makeConnect(DBConfig::HOSTNAME, DBConfig::DB_USER, DBConfig::DB_PASS, DBConfig::DB_NAME)
    @source = "diadiem"
    @linkTable = "#{@city}_#{@source}_link"
    @detailTable = "#{@city}_#{@source}_detail"
  end

  def createTable
    hash = {
      "link"      => "varchar(500)",
      "state"     => "varchar(10)",
      "category"  => "varchar(100)",
    }

    Crawl.createTableByHash(@con, @linkTable, hash)

    hash = {
      "name"           => "varchar(500)",
      "address"        => "varchar(500)", 
      "phone"          => "varchar(200)",
      "website"        => "varchar(200)",
      "email"          => "varchar(200)",
      "type"           => "varchar(200)",
      "longtitude"     => "varchar(200)",
      "latitude"       => "varchar(200)",
      "category"       => "varchar(100)",
      "link"           => "varchar(500)",
    }
    Crawl.createTableByHash(@con, @detailTable, hash)
  end

  def getLink
    # Empty table
    #@con.query ("truncate table `#{@linkTable}`")
    
    # Config Selenium Browser
    wait                                      = Selenium::WebDriver::Wait.new(:timeout => 15)  # seconds
    profile                                   = Selenium::WebDriver::Firefox::Profile.from_name "default"
    profile['permissions.default.stylesheet'] = 2
    driver                                    = Selenium::WebDriver.for(:firefox, :profile => profile) #
    # Get first link from database
    res_arg = @con.query ("select * from `#{@source}_arguments` where `province` = '#{@city}' ")
    i = 0
    res_arg.each_hash do |arg_row|
      initialink = arg_row['argument']
      keyword = @con.query ("select * from `category_keyword`")
      
        driver.navigate.to initialink
      sleep(1)
      keyword.each_hash do |arg_row_key|
          initiakey = arg_row_key['keyword']
          initiacategory = arg_row_key['category']
          #Get the first link (or any element you want)
          element = driver.find_element(:css, 'input#txtSearchNearBy')
          #puts element.attribute('value')
          script = " return arguments[0].value = '#{initiakey}' "
          script.force_encoding("utf-8")
          driver.execute_script(script, element)
          puts element.attribute('value')

          begin
          element = driver.find_element(:xpath, "//a[@id='btnSearchYP']")
          element.click()
          sleep(3)
          rescue Exception => e
            puts e.message
            break
          end
          string = driver.page_source
           wait.until{
            begin
              doc = Nokogiri::HTML(string)
              hash = Hash.new
              hash['category'] = initiacategory
              puts hash['link'] = "http://www.diadiem.com/" + doc.css("form[name = 'form1']").attr('action')
              puts i = i+1
              Crawl.genDataInsert(@con, hash, @linkTable)
              break
            rescue Exception => e
              puts e.message
              puts e.backtrace.inspect
            end
           }
           puts "------------------------"
      end
    end
    puts "Finish get link!"
    driver.quit
  end

  def getDetail
    # Empty table
    #@con.query ("truncate table `#{@detailTable}`")
    # Config Selenium Browser
    wait                                      = Selenium::WebDriver::Wait.new(:timeout => 100)  # seconds
    profile                                   = Selenium::WebDriver::Firefox::Profile.from_name "default"
    profile['permissions.default.stylesheet'] = 2
    driver                                    = Selenium::WebDriver.for(:firefox, :profile => profile) #
    # Get first link from database
    res_arg = @con.query ("select * from `#{@linkTable}`")
      keypage = 0
      res_arg.each_hash do |arg_row|
      initialink = arg_row['link']
      initiacategory = arg_row['category']
      initiaid = arg_row['id']
      
      driver.navigate.to initialink
      page = 1
      sleep(3)
      while true
        begin
          link = ''
          string = driver.page_source
            if page == 1
              begin
              element = driver.find_element(:xpath, "//a[@id='divPaging_Next']")
              element.click()
              sleep(1)
              element = driver.find_element(:xpath, "//a[@id='divPaging_Back']")
              element.click()
              sleep(1)
              string = driver.page_source
              rescue Exception => e
                puts "Khong co trang ke tiep"
              end
            end

          page = page + 1
          wait.until{
            begin
              doc = Nokogiri::HTML(string)
              doc.css("div[class='detail_body']").map{|item|
                hash = Hash.new
                hash['name']        = item.css("h5[class='detail_title']")[0].css("a")[0].inner_text
                addr                = item.css("div[class='detail_info']")[0].css("p")[0].inner_text
                addr                += ", "
                addr                += item.css("div[class='detail_info']")[0].css("p")[1].inner_text
                hash['address']     = addr
                hash['category']    = initiacategory
                hash['type']        = item.css("a[class='detail_type']").inner_text
                
                hash['latitude']    = item.css("div[class='detail_info']").css("a[class='detail_type']")[0]['id'].split("_")[4]
                hash['longtitude']  = item.css("div[class='detail_info']").css("a[class='detail_type']")[0]['id'].split("_")[5]
                hash['link']        = "http://www.diadiem.com/" + item.css("h5[class='detail_title']")[0].css("a")[0].attr('href')
                puts "*****************"
                puts page - 1
                puts hash['name']
                puts hash['type']

                #puts hash['address']
                #puts hash['latitude']
                #puts hash['longtitude']
                Crawl.genDataInsert(@con, hash, @detailTable)
                
              }
            rescue Exception => e
              puts e.message
              #puts e.backtrace.inspect
              puts "Khong xac dinh phan tu!"
              break
            end
          }

          begin
            element = driver.find_element(:xpath, "//a[@id='divPaging_Next'][1]")
            element.click()
            sleep(2)
          rescue Exception => e
            #puts e.message
            puts 'Ket thuc tu khoa' 
            puts initiaid
            break
          end  
        # rescue Exception => e
        #   puts e.message
        #   puts e.backtrace.inspect
        #   puts 'LOI '+ page
        #   break
          end
      end
      next
    end
    driver.quit
  end

end